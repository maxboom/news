## Советы по установке 

Склонировать репозиторий и перейти в директорию проекта
```
git clone https://gitlab.com/maxboom/news.git
```

Скопировать настройки енваирмента и перейти к установке зависимостей
```
cp .env.example .env

docker run --rm \
-u "root:root" \
-v $(pwd):/var/www/html \
-w /var/www/html \
laravelsail/php81-composer:latest \
composer install --ignore-platform-reqs
```

Опционально:
```
./vendor/bin/sail down --rmi all -v
```

Запускаем контейнер
```
./vendor/bin/sail up
```

В отдельном терминале:
```
chmod 777 -R storage/

./vendor/bin/sail root-bash

php artisan migrate

npm install
npm run build
```

go to: http://localhost/
