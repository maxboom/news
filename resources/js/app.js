import './bootstrap';
import '../css/app.css';

import { createApp } from 'vue';
import Example from "./components/Index.vue";
import router from "./router/index.js";

const app = createApp({
    components: {
        Example,
    }
});

app.use(router);

app.mount("#app");
