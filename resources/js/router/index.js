import {createRouter, createWebHistory} from "vue-router";
import Index from "../components/Index.vue";
import Show from "../components/Show.vue";
import News from "../components/News.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'Home',
        },
        {
            path: '/news',
            name: 'News',
            component: News,
        },
        {
            path: '/news/:id',
            name: 'Show',
            component: Show,
            props: true,
        }
    ]
});

export default router;
