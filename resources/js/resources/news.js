import { ref } from "vue";
import axios from "axios";

export default function useNews() {
    const news = ref([]);
    const currentNews = ref([]);

    const getNews = async () => {
        const response = await axios.get('/api/v1/news');
        news.value = response.data.data;
    }

    const getCurrentNews = async (id) => {
        const response = await axios.get('/api/v1/news/' + id);
        currentNews.value = response.data.data;
    }

    const changeStatus = async (id) => {
        const response = await axios.get('/api/v1/news/' + id + '/active');
        currentNews.value = response.data.data;
    }

    return {
        news,
        currentNews,
        getNews,
        getCurrentNews,
        changeStatus,
    };
}
