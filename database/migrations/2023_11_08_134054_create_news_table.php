<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('news');

        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->text('content');
            $table->boolean('active')->default(false);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();
        });

        DB::table('news')->insert([
            [
                'title' => 'Title 1',
                'description' => 'Description 1',
                'content' => 'Content 1',
                'active' => true,
            ],
            [
                'title' => 'Title 2',
                'description' => 'Description 2',
                'content' => 'Content 2',
                'active' => true,
            ],
            [
                'title' => 'Title 3',
                'description' => 'Description 3',
                'content' => 'Content 3',
                'active' => true,
            ],
        ]);

        DB::table('news')->insert([
            [
                'title' => 'Title 4',
                'description' => 'Description 4',
                'content' => 'Content 4',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('news');
    }
};
