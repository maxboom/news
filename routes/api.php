<?php

use App\Http\Controllers\NewsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1'], function () {
    Route::apiResource('news', NewsController::class)->only(['index', 'show']);
    Route::get('news/{id}/active', [NewsController::class, 'active']);
});
