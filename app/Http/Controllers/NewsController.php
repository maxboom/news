<?php

namespace App\Http\Controllers;

use App\Http\Resources\NewsCollection;
use App\Http\Resources\NewsResource;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new NewsCollection(News::where('active', true)->get());
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return new NewsResource(News::findOrFail($id));
    }

    /**
     * Toggle status of the resource
     */
    public function active(string $id)
    {
        $news = News::findOrFail($id);

        $news->active = !$news->active;

        $news->save();

        return new NewsResource(News::findOrFail($id));
    }
}
